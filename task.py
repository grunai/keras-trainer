import os
from enum import Enum

import tensorflow as tf

from . import model
from . import util
from . import config

Action = Enum('Action', 'create load')
behavior = Action.create

def train(keras_model):
  _ = keras_model.fit(
    util.data_generator(),
    epochs=50,
    steps_per_epoch=1000,
    callbacks=[
      tf.keras.callbacks.EarlyStopping(monitor='iou', patience=3, mode='max'),
      tf.keras.callbacks.LearningRateScheduler(model.lr_schedule)
    ],
    verbose=2
  )

  return keras_model


if __name__ == '__main__':
  keras_model =  None
  if behavior == Action.create:
    keras_model = model.create_keras_model()
  elif behavior == Action.load:
    keras_model = model.load_keras_model(config.SAVE_PATH)

  keras_model = train(keras_model)

  keras_model.save(config.SAVE_PATH)
